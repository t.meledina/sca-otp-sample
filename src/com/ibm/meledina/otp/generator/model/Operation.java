package com.ibm.meledina.otp.generator.model;

import com.google.gson.Gson;

public class Operation {
    private String beneficiary;
    private Double amount;

    public Operation() {}

    public Operation(String beneficiary, Double amount) {
        this.beneficiary = beneficiary;
        this.amount = amount;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}

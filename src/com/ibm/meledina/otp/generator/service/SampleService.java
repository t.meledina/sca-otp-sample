package com.ibm.meledina.otp.generator.service;

import com.ibm.meledina.otp.generator.db.FakeDatabase;
import com.ibm.meledina.otp.generator.model.Operation;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

public class SampleService {

    private Base64.Encoder base64encoder = Base64.getEncoder();

    private static final String pbkdf2Algorithm = "PBKDF2WithHmacSHA256";
    private static final int encryptionStrength = 100000;
    private static final int keyLength = 128;
    private static final int otpLength = 8;
    private static final String humanlyUndesiredCharsPattern = "[^A-Z 0-9a-z]";

    public String  verify(String transactionId, Operation operation) throws InvalidKeySpecException, NoSuchAlgorithmException {
        System.out.println("Received operation " + operation + " to verify, with transactionId " + transactionId);
        byte[] salt = generateRandomSalt();

        //System.out.println("Generated salt " + base64encoder.encodeToString(salt) + " with transactionId " + transactionId + ", writing it into the (fake) database");
        FakeDatabase.transactionSaltTable.put(transactionId, salt);
        String otp = generateOTP(transactionId, operation);
        System.out.println("Generated OTP " + otp + " and sent to customer...");

        return  otp;
    }

    public boolean confirm(String transactionId, Operation operation, String otp) throws InvalidKeySpecException, NoSuchAlgorithmException {
        System.out.println("Received operation " + operation + " to confirm, with transactionId " + transactionId + ", and OTP " + otp);
        byte[] salt = FakeDatabase.transactionSaltTable.get(transactionId);
        System.out.println("Generating OTP from scratch...");
        String regeneratedOtp = generateOTP(transactionId, operation);

        if(otp.equals(regeneratedOtp)) {
            System.out.println("OTP " + otp + " received from user matches with regenerated OTP " + regeneratedOtp + ", authorizing!");
            return true;
        }
        else {
            System.out.println("OTP received from user " + otp + " does NOT match with regenerated OTP " + regeneratedOtp + ", denying!");
            return false;
        }
    }



    private String humanizeOTP(String originalHash) {
        return originalHash.replaceAll(humanlyUndesiredCharsPattern, "").substring(0, otpLength);
    }

    private byte[] generateRandomSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);

        return salt;
    }

    private String generateOTP(String transactionId, Operation operation) throws NoSuchAlgorithmException, InvalidKeySpecException {

        byte[] salt = FakeDatabase.transactionSaltTable.get(transactionId);
        //System.out.println("Retrieved salt " + base64encoder.encodeToString(salt) + " for transactionId " + transactionId);

        return humanizeOTP(generateSecureHash(operation.toString(), salt));
    }

    private String generateSecureHash(String seed, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeySpec spec = new PBEKeySpec(seed.toCharArray(), salt, encryptionStrength, keyLength);
        SecretKeyFactory factory = SecretKeyFactory.getInstance(pbkdf2Algorithm);

        byte[] hash = factory.generateSecret(spec).getEncoded();

        return base64encoder.encodeToString(hash);
    }
}

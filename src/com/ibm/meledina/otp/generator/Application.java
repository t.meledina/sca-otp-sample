package com.ibm.meledina.otp.generator;

import com.ibm.meledina.otp.generator.model.Operation;
import com.ibm.meledina.otp.generator.service.SampleService;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

public class Application {

    private static final int nSampleIterations = 20;

    public static void main(String[] args) {

        Operation dummyOp = new Operation("Amnesty International", 1000.0);

        SampleService sampleService = new SampleService();

        for(int i=0; i<nSampleIterations;i++) {
            long startTime = System.currentTimeMillis();
            try {
                String dummyTransactionId = UUID.randomUUID().toString();
                String otpSentToUser = sampleService.verify(dummyTransactionId, dummyOp);
                System.out.println("User sends back otp " + otpSentToUser);
                sampleService.confirm(dummyTransactionId, dummyOp, otpSentToUser);

            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }
            System.out.println("Validated in " + (System.currentTimeMillis()-startTime) + " ms\n");
        }

    }
}

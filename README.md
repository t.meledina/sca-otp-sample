#### PSD2-compliant OTP generator POC
This small application supposedly demonstrates how to build short, human-viable OTPs that comply with the most recent (May 2019) EU regulations regarding Strong Customer Authentication i.e. dynamic linking.

For each operation sent to the backend (i.e. this application), identified with a _transactionId_, a random _salt_ is generated and then used in order to build a PBE hash, then only the first 8 humanly-viable characters (customizable) are taken and used as OTP.

Validation is easily possible because of the deterministic nature of the PBE hash generation algorithm.

FYI here are the [chance of collision](https://www.wolframalpha.com/input/?i=1.0*(1%2F62)%5En,+n%3D4+to+10) for _a-z A-Z 0-9_ characters as the number of digits increases:

- n=4 - 6.76758×10^-8
- n=5 - 1.09154×10^-9
- n=6 - 1.76056×10^-11
- n=7 - 2.83961×10^-13
- n=8 - 4.58001×10^-15
- n=9 - 7.38711×10^-17
- n=10 - 1.19147×10^-18 